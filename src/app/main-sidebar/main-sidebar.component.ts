import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  styleUrls: ['./main-sidebar.component.scss']
})
export class MainSidebarComponent implements OnInit {
  array = [
    { "title": "WordPress Version", "details": "5.5.1" },
    { "title": "Debug Mode", "details": "Disable" },
    { "title": "PHP Version", "details": "7.2.34" },
    { "title": "MainWP Version", "details": "4.1" },
    { "title": "PHP Memory Limit", "details": "128M" },
    { "title": "MySQL Version", "details": "5.7.32" },
    { "title": "Activated Theme", "details": "Divi Child Theme" },
    { "title": "Server IP", "details": "NA" }
  ];
  recentPost = [
    { "title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
    { "title": "Placeholder text commonly used in the graphic  ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
    { "title": "Lorem ipsum is placeholder text Real Estate commonly used in the graphic  ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
  ];
  recentPages = [
    { "title": " Thank You ", "date": " October 20, 2020 (MST) " },
    { "title": " Home ", "date": " October 19, 2020 (MST) " },
    { "title": " Privacy Policy ", "date": " December 24, 2019 (MST)  " },
  ];
  sidebarNames = [
    { "name": "Overview", "icon": "fas fa-th" },
    { "name": "Plugins", "icon": "fa fa-plug" },
    { "name": "Themes", "icon": "fa fa-image" },
    { "name": "Review This Theme", "icon": "fa fa-star" },
    { "name": "Blog Add-on", "icon": "fa fa-edit" },
    { "name": "Location Page Add-on", "icon": "fa fa-map-marker" },
    { "name": "SEO Add-on", "icon": "fa fa-search" },
    { "name": "SMO Add-on", "icon": "fa fa-comments" },
    { "name": "PPC Add-on", "icon": "fas fa-search-dollar" },
  ];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  navigate(event) {
    console.log('Navigate Page-->', event);
    if (event == 'Overview') {
      this.router.navigate(['/']);
    } else if (event == 'Plugins') {
      this.router.navigate(['/plugin']);
    } else if (event == 'Themes') {
      this.router.navigate(['/themes']);
    } else if (event == 'Review This Theme') {
      this.router.navigate(['/reviewTheme']);
    } else if (event == 'Blog Add-on') {
      this.router.navigate(['/blogAddOn']);
    } else if (event == 'Location Page Add-on') {
      this.router.navigate(['/locationPageAddOn']);
    } else if (event == 'SEO Add-on') {
      this.router.navigate(['/seoAddOn']);
    } else if (event == 'SMO Add-on') {
      this.router.navigate(['/smoAddOn']);
    } else if (event == 'PPC Add-on') {
      this.router.navigate(['/ppcAddOn']);
    }
  }
}
