import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PpcAddOnComponent } from './ppc-add-on.component';

describe('PpcAddOnComponent', () => {
  let component: PpcAddOnComponent;
  let fixture: ComponentFixture<PpcAddOnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PpcAddOnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PpcAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
