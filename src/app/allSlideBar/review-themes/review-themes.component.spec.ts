import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewThemesComponent } from './review-themes.component';

describe('ReviewThemesComponent', () => {
  let component: ReviewThemesComponent;
  let fixture: ComponentFixture<ReviewThemesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewThemesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewThemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
