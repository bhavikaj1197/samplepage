import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogAddOnComponent } from './blog-add-on.component';

describe('BlogAddOnComponent', () => {
  let component: BlogAddOnComponent;
  let fixture: ComponentFixture<BlogAddOnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogAddOnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
