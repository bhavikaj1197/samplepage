import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeoAddOnComponent } from './seo-add-on.component';

describe('SeoAddOnComponent', () => {
  let component: SeoAddOnComponent;
  let fixture: ComponentFixture<SeoAddOnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeoAddOnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeoAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
