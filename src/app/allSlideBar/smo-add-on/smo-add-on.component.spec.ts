import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmoAddOnComponent } from './smo-add-on.component';

describe('SmoAddOnComponent', () => {
  let component: SmoAddOnComponent;
  let fixture: ComponentFixture<SmoAddOnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmoAddOnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmoAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
