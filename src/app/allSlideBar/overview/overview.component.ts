import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  array = [
    { "title": "WordPress Version", "details": "5.5.1" },
    { "title": "Debug Mode", "details": "Disable" },
    { "title": "PHP Version", "details": "7.2.34" },
    { "title": "MainWP Version", "details": "4.1" },
    { "title": "PHP Memory Limit", "details": "128M" },
    { "title": "MySQL Version", "details": "5.7.32" },
    { "title": "Activated Theme", "details": "Divi Child Theme" },
    { "title": "Server IP", "details": "NA" }
  ];
  recentPost = [
    { "title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
    { "title": "Placeholder text commonly used in the graphic  ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
    { "title": "Lorem ipsum is placeholder text Real Estate commonly used in the graphic  ", "category": " Real Estate", "date": " October 15, 2020 (MST) " },
  ];
  recentPages = [
    { "title": " Thank You ", "date": " October 20, 2020 (MST) " },
    { "title": " Home ", "date": " October 19, 2020 (MST) " },
    { "title": " Privacy Policy ", "date": " December 24, 2019 (MST)  " },
  ];
  constructor() { }

  ngOnInit(): void {
  }
  editrecentPages() {
    console.log('Edit Recent Pages code here-->');
  }
  editrecentPost() {
    console.log('Edit Recent Post code here-->');
  }

}
