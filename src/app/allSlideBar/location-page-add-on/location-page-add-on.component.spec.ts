import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationPageAddOnComponent } from './location-page-add-on.component';

describe('LocationPageAddOnComponent', () => {
  let component: LocationPageAddOnComponent;
  let fixture: ComponentFixture<LocationPageAddOnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationPageAddOnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationPageAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
