import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainSidebarComponent } from './main-sidebar/main-sidebar.component';
import { PluginComponent } from './allSlideBar/plugin/plugin.component';
import { OverviewComponent } from './allSlideBar/overview/overview.component';
import { ThemesComponent } from './allSlideBar/themes/themes.component';
import { ReviewThemesComponent } from './allSlideBar/review-themes/review-themes.component';
import { BlogAddOnComponent } from './allSlideBar/blog-add-on/blog-add-on.component';
import { LocationPageAddOnComponent } from './allSlideBar/location-page-add-on/location-page-add-on.component';
import { SeoAddOnComponent } from './allSlideBar/seo-add-on/seo-add-on.component';
import { SmoAddOnComponent } from './allSlideBar/smo-add-on/smo-add-on.component';
import { PpcAddOnComponent } from './allSlideBar/ppc-add-on/ppc-add-on.component';

@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent,
    MainSidebarComponent,
    PluginComponent,
    OverviewComponent,
    ThemesComponent,
    ReviewThemesComponent,
    BlogAddOnComponent,
    LocationPageAddOnComponent,
    SeoAddOnComponent,
    SmoAddOnComponent,
    PpcAddOnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
