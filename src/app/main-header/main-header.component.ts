import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {
  emailId = "https://sandeshtestingpages.com";
  constructor() { }

  ngOnInit(): void {
  }
  preview(){
    // Preview code here
    console.log('Preview code here');
  }
  publish(){
    // Publish code here
    console.log('Publish code here');
  }
  webAdmin(){
    // Web Admin code here
    console.log('Web Admin code here');
  }
  signIn() {
    // For sign Out code here-->
    console.log('For sign Out code here-->')
  }
}
