import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogAddOnComponent } from './allSlideBar/blog-add-on/blog-add-on.component';
import { LocationPageAddOnComponent } from './allSlideBar/location-page-add-on/location-page-add-on.component';
import { OverviewComponent } from './allSlideBar/overview/overview.component';
import { PluginComponent } from './allSlideBar/plugin/plugin.component';
import { PpcAddOnComponent } from './allSlideBar/ppc-add-on/ppc-add-on.component';
import { ReviewThemesComponent } from './allSlideBar/review-themes/review-themes.component';
import { SeoAddOnComponent } from './allSlideBar/seo-add-on/seo-add-on.component';
import { SmoAddOnComponent } from './allSlideBar/smo-add-on/smo-add-on.component';
import { ThemesComponent } from './allSlideBar/themes/themes.component';

const routes: Routes = [
  { path: '', component: OverviewComponent},
  { path: 'plugin', component: PluginComponent},
  { path: 'themes', component: ThemesComponent},
  { path: 'reviewTheme', component: ReviewThemesComponent},
  { path: 'blogAddOn', component: BlogAddOnComponent},
  { path: 'locationPageAddOn', component: LocationPageAddOnComponent},
  { path: 'seoAddOn', component: SeoAddOnComponent},
  { path: 'smoAddOn', component: SmoAddOnComponent},
  { path: 'ppcAddOn', component: PpcAddOnComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
